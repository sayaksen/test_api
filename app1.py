from flask import Flask
from flask import request
from flask import jsonify
import sqlite3
from flask import g
app = Flask(__name__)
import json
from flask_httpauth import HTTPBasicAuth


auth = HTTPBasicAuth()
USER_DATA = {
    "root": "password"
}


con = sqlite3.connect("Gaul_Town")
cur = con.cursor()
cur.execute("SELECT * from 'credentials'")
rows = cur.fetchall()
for row in rows:
	USER_DATA[row[0]]=row[1]


@auth.verify_password
def verify(username, password):
    if not (username and password):
        return False
    return USER_DATA.get(username) == password


@app.route('/')
def index():
	return jsonify({"status":"Working"})


@app.route('/gauls/<id_>',methods=['GET'])
@auth.login_required
def gauls(id_):
	print(id_)
	if (id_==None): return allgauls()
	else: return gaul(id_)


def gaul(id_):
	con = sqlite3.connect("Gaul_Town")
	cur = con.cursor()

	if (id_ != None): 
		cur.execute("SELECT * from 'Citizens' WHERE id = ?;",(id_,))
		row = cur.fetchone()
		txt = {
			"id":row[0],
			"fname":row[1],
			"lname":str(row[2]),
			"contact":row[3] 
		}

	con.close()
	return jsonify(txt)


def allgauls():
	con = sqlite3.connect("Gaul_Town")
	cur = con.cursor()
	cur.execute("SELECT * from 'Citizens';")
	rows = cur.fetchall()
	print(len(rows))
	count = 0
	txt={}
	for row in rows:
		print(count)
		txt[count] = {
					"id":row[0],
					"fname":row[1],
					"lname":str(row[2]),
					"contact":row[3]
				}
		count+=1
	con.close()
	return jsonify(txt)


@app.route('/add',methods=['POST'])
@auth.login_required
def add_():
	con = sqlite3.connect("Gaul_Town")
	cur = con.cursor()
	first_name_ = serialize(request.args.get('first_name'))
	last_name_ = serialize(request.args.get('last_name'))
	contact_ = serialize(request.args.get('contact'))

	if (first_name_==None): return "First Name is required"
	if (contact_==None): return "Contact number is needed"
	if (len(contact_)!= 10 or (not contact_.isdigit())): return "Please enter correct contact number"

	try: 
		cur.execute("INSERT INTO 'Citizens' VALUES (?, ?, ?, ?)", (None, first_name_,last_name_,contact_))
		con.commit()
	except Exception as e:
		con.close()
		return str(e)

	cur.execute("SELECT * from 'Citizens' WHERE contact_number = ?;",(contact_,))
	id_ = (cur.fetchone())[0]
	cur.close()
	return gaul(id_)


@app.route('/remove/<id_>',methods=['DELETE'])
@auth.login_required
def remove(id_):
	con = sqlite3.connect("Gaul_Town")
	cur = con.cursor()
	id_ = serialize(id_)
	#id_ = request.args.get('id')
	#contact_ = request.args.get('contact')

	if (id_ != None): 
		cur.execute("DELETE from 'Citizens' WHERE id = ?;",(id_,))
		con.commit()
		con.close()
		return "Gaul-ID = "+ id_ + " is no more a part of Gaul Family"
	else: return "Please enter a valid Gaul-ID"


@app.route('/update/<id_>',methods=['PUT'])
@auth.login_required
def update():
	con = sqlite3.connect("Gaul_Town")
	cur = con.cursor()
	#id_ = request.args.get('id')
	first_name = serialize(request.args.get('fname'))
	last_name = serialize(request.args.get('lname'))
	contact_ = serialize(request.args.get('contact'))

	if (id_ != None):
		try: 
			cur.execute("UPDATE 'Citizens' SET first_name = ?, last_name = ?, contact_number = ? WHERE id = ?;",(first_name,last_name,contact_,id_))
			con.commit()
			con.close()
		except Exception as e:
			con.close()
			return str(e)	
	else: return "Please enter a valid Gaul-ID"
	return gaul(id_)


def serialize(query):
	query = query.replace(";","")
	query = query.replace("'","")
	return query
	


if __name__ == '__main__':
	app.run(debug=True)
